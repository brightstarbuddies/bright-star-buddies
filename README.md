At Bright Star Buddies we always make sure our products are not only adorable but more importantly super durable.
Our range of Dog accessories including dog tags, dog collars, dog bandanas and dog clothes always comes out on top! Make your own today!

Address: P.O Box 1179, Wollongong, NSW 2500, Australia

Phone: +61 1300 414 534

Website: https://www.brightstarbuddies.com.au
